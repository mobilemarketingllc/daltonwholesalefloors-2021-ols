var $menu = jQuery('.searchIcon, .searchModule');

jQuery(document).mouseup(function (e) {
if (!$menu.is(e.target) && $menu.has(e.target).length === 0 && jQuery('.searchModule').css('display') == 'block') {
jQuery('.searchModule').slideToggle();
}
});

jQuery('.searchIcon').click(function(){
    
    jQuery('.searchModule').slideToggle();
    
})  

jQuery(window).scroll(function(){
    if (jQuery(this).scrollTop() > 164) {
        jQuery('.fixed-header').addClass('scroll-none');
        jQuery('.sticky-header').addClass('sticky-header-active');
    } else {
        jQuery('.fixed-header').removeClass('scroll-none');
        jQuery('.sticky-header').removeClass('sticky-header-active');
    }
});




jQuery(document).ready(function() {

    jQuery.ajax({
       type: "POST",
       url: "/wp-admin/admin-ajax.php",
       data: 'action=get_storelisting',
       dataType: 'JSON',
       success: function(response) {
          
               var posts = JSON.parse(JSON.stringify(response));
               var header_data = posts.header;
               var list_data = posts.list;

               var mystore_loc =  jQuery.cookie("preferred_storename");
               
             
              // jQuery(".locationSelector .uabb-marketing-title").html(header_data);
               jQuery(".header_location_name").html(header_data);
             //  setTimeout(addFlyerEvent, 1000);
               jQuery("#ajaxstorelisting").html(list_data);
               //iconsClick();      
       }
   });

});
   jQuery(document).on('click', '.choose_location', function() {
  
    var mystore = jQuery(this).attr("data-id");
    var distance = jQuery(this).attr("data-distance");
    var storename = jQuery(this).attr("data-storename");

    jQuery.cookie("preferred_store", null, { path: '/' });
    jQuery.cookie("preferred_distance", null, { path: '/' });
    jQuery.cookie("preferred_storename", null, { path: '/' });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=choose_location&store_id=' + jQuery(this).attr("data-id") + '&distance=' + jQuery(this).attr("data-distance"),

        success: function(data) {

            jQuery(".header_location_name").html(data);
            jQuery.cookie("preferred_store", mystore, { expires: 1, path: '/' });
            jQuery.cookie("preferred_distance", distance, { expires: 1, path: '/' });
            jQuery.cookie("preferred_storename", storename, { expires: 1, path: '/' });

            jQuery(".uabb-offcanvas-close").trigger("click");
           
        }
    });

});

jQuery(document).ready(function() {

    var mystore_loc =  jQuery.cookie("preferred_storename");
    console.log(mystore_loc);
    jQuery(".populate-store select").val(mystore_loc);
    
    jQuery(".facetwp-per-page-select").attr("aria-label","perpage");

});

jQuery( document ).ready(function() {
	if(jQuery(".fl-content.product.col-sm-12 > div").hasClass("product-detail-layout-6")){ jQuery('body').addClass("pdpPage");}
});

jQuery( document ).ready(function() {
	if(jQuery(".fl-page-content > div").hasClass("fl-archive")){ jQuery('body').addClass("blog");}
});



